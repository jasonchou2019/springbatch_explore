package com.jason.processor.common;

import org.springframework.batch.item.ItemProcessor;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;

/************************************************************************************************
 *  @Author:zhouhx
 *  @Date: 9:56 2020/11/18                                                                                                                                                 
 *  @Description: //公共处理类
 *  @Version:1.0
 ************************************************************************************************
 **/
public abstract class CommonProcessor<I,O> implements ItemProcessor<I,O> {

    private Class<I> input;

    private Class<O> output;

    @PostConstruct
    public void init() {
        input = (Class<I>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        output = (Class<O>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    @Override
    public O process(I i) throws Exception {
        O o = output.newInstance();
        processor(o,i);
        return o;
    }

    public abstract void processor(O o, I i);
}
