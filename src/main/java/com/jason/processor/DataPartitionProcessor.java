package com.jason.processor;

import com.jason.entity.DataInput;
import com.jason.entity.DataOutput;
import com.jason.processor.common.CommonProcessor;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.stereotype.Component;

/************************************************************************************************
 *  @Author:zhouhx
 *  @Date: 9:54 2020/11/18                                                                                                                                                 
 *  @Description: //分区公共处理
 *  @Version:1.0
 ************************************************************************************************
 **/
@Component
@StepScope
public class DataPartitionProcessor extends CommonProcessor<DataInput, DataOutput> {

    @Override
    public void processor(DataOutput o, DataInput i) {
        o.setAddress(i.getAddress());
        o.setAge(i.getAge());
        o.setSex(i.getSex());
        o.setName(i.getName());
    }
}
