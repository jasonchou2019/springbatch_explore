package com.jason.test;

import java.sql.*;

public class DataInsertTest {

    private static String url = "jdbc:mysql://localhost/demo?useUnicode=true&rewriteBatchedStatements=true&characterEncoding=UTF-8&serverTimezone=UTC";
    private static String user = "root";
    private static String password = "123456";

    public static void main(String[] args) {

        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, password);
            String sql = "INSERT INTO tb_input(name,age,sex,address) values(?,?,?,?)";
            pstm = conn.prepareStatement(sql);
            Long startTime = System.currentTimeMillis();
            conn.setAutoCommit(false);
            for (int i = 1; i <= 3000000; i++) {
                pstm.setString(1, "张三"+(i));
                pstm.setInt(2,(i));
                pstm.setString(3, isOdd(i)?"男":"女");
                pstm.setString(4,"解放路"+(i)+"号");
                pstm.addBatch();
            }
            pstm.executeBatch();
            conn.commit();
            Long endTime = System.currentTimeMillis();
            System.out.println("用时：" + (endTime - startTime));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (pstm != null) {
                try {
                    pstm.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public static boolean isOdd(int number){
        if((number&1) != 1){   //是奇数
            return true;
        }
        return false;
    }
}