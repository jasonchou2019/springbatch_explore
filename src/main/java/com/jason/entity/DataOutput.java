package com.jason.entity;

import java.io.Serializable;

/************************************************************************************************
 *  @Author:zhouhx
 *  @Date: 10:27 2020/11/18                                                                                                                                                 
 *  @Description: //TODO                                                                                 
 *  @Version:1.0
 ************************************************************************************************
 **/
public class DataOutput implements Serializable {

    private Integer id;

    private String name;

    private int age;

    private String sex;

    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
