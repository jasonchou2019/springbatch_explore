package com.jason;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/************************************************************************************************
 *  @Author:zhouhx
 *  @Date: 17:30 2020/11/17                                                                                                                                                 
 *  @Description: //启动类
 *  @Version:1.0
 ************************************************************************************************
 **/
@SpringBootApplication(scanBasePackages = {"com.jason"})
@EnableTransactionManagement(proxyTargetClass = true)
@MapperScan(basePackages = {"com.jason.**.mapper"})
public class SpringBatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBatchApplication.class, args);
    }
}
