package com.jason.reader;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisPagingItemReader;

import java.util.HashMap;
import java.util.Map;

/************************************************************************************************
 *  @Author:zhouhx
 *  @Date: 9:49 2020/11/18                                                                                                                                                 
 *  @Description: //spring batch公用reader
 *  @Version:1.0
 ************************************************************************************************
 **/
public class CommonPartitionMybatisItemReader<T> extends MyBatisPagingItemReader<T> {

    public CommonPartitionMybatisItemReader(SqlSessionFactory sqlSessionFactory, String name, String fromId, String toId) {
        setSqlSessionFactory(sqlSessionFactory);
        setQueryId("com.jason.entity."+name+".selectPartitionList");
        Map<String,Object> parameterValues = new HashMap<>();
        parameterValues.put("fromId", fromId);
        parameterValues.put("toId", toId);
        setParameterValues(parameterValues);
        setPageSize(500);
    }
}
