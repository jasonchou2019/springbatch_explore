package com.jason.mapper;

import org.springframework.stereotype.Repository;

@Repository
public interface DataMapper {

    int getInputMaxId();

    int getInputMinId();
}
