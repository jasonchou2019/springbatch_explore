package com.jason.mapper;

import com.jason.entity.DataOutput;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataOutputMapper {

    int batchInsert(List<DataOutput> list);
}
