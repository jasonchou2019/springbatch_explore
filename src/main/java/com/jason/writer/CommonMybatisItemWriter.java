package com.jason.writer;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;

/************************************************************************************************
 *  @Author:zhouhx
 *  @Date: 9:51 2020/11/18                                                                                                                                                 
 *  @Description: //公共写数据库
 *  @Version:1.0
 ************************************************************************************************
 **/
public class CommonMybatisItemWriter<T> extends MyBatisBatchItemWriter<T> {

    public CommonMybatisItemWriter(SqlSessionFactory sqlSessionFactory, String name) {
        setSqlSessionFactory(sqlSessionFactory);
        setStatementId("com.jason.mapper." + name + ".insert");
        setAssertUpdates(false);
    }
}
