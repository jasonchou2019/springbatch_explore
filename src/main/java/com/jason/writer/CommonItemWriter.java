package com.jason.writer;

import com.jason.entity.DataOutput;
import com.jason.mapper.DataOutputMapper;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CommonItemWriter implements ItemWriter<DataOutput> {

    @Autowired
    private DataOutputMapper outputMapper;

    @Override
    public void write(List<? extends DataOutput> items) throws Exception {
        List<DataOutput> outputs=(List<DataOutput>)items;
        outputMapper.batchInsert(outputs);
    }
}
