package com.jason.service;

import com.jason.mapper.DataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataInputService implements IDataInputService{

    @Autowired
    private DataMapper dataMapper;

    @Override
    public int getInputMaxId() {
        return dataMapper.getInputMaxId();
    }

    @Override
    public int getInputMinId() {
        return dataMapper.getInputMinId();
    }
}
