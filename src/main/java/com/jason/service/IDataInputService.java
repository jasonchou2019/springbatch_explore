package com.jason.service;

public interface IDataInputService {

    int getInputMaxId();

    int getInputMinId();

}
