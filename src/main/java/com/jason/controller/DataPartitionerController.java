package com.jason.controller;

import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

/************************************************************************************************
 *  @Author:zhouhx
 *  @Date: 10:13 2020/11/18                                                                                                                                                 
 *  @Description: //分区controller
 *  @Version:1.0
 ************************************************************************************************
 **/
@RestController
public class DataPartitionerController {

    @Autowired
    private JobLauncher jobLauncher;

    @Resource
    private Job catPartitionJob;


    @GetMapping("/job/step")
    public void execute() {

        JobParameters jobParameters = new JobParametersBuilder()
                .addLong("currentTimeMillis", System.currentTimeMillis())
                .addString("uuid", UUID.randomUUID().toString().replace("-" ,"").toUpperCase())
                .toJobParameters();

        try {
            JobExecution job = jobLauncher.run(catPartitionJob, jobParameters);
            System.out.println(job.getExitStatus());
        } catch (JobExecutionAlreadyRunningException e) {
            e.printStackTrace();
        } catch (JobRestartException e) {
            e.printStackTrace();
        } catch (JobInstanceAlreadyCompleteException e) {
            e.printStackTrace();
        } catch (JobParametersInvalidException e) {
            e.printStackTrace();
        }
    }

}
